<?php

use App\Http\Controllers\UsuariosController;
use App\Models\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/prueba', function () {
//     return view('test.prueba');
// });

Route::get('/usuarios', [UsuariosController::class, 'index']);

Route::get('/crear', function () {
    $user = User::create([
        'name' => "Danny",
        'email' => "danny@email.com",
        'password' => bcrypt('123456')
    ]);

    if ($user) {
        return $user;
    }

    return "ocurrió un error";
});

//get post put patch delete
